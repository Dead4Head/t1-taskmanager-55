package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLogoutRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLoginResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLogoutResponse;
import ru.t1.amsmirnov.taskmanager.marker.SoapCategory;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final String login = "test";

    @NotNull
    private final String password = "test";


    @Test
    public void testLoginAndLogout() {
        @NotNull UserLoginRequest request = new UserLoginRequest(NONE_STR, NONE_STR);
        @NotNull UserLoginResponse response = authEndpoint.login(request);
        assertNull(response.getToken());
        assertFalse(response.isSuccess());
        assertNotNull(response.getMessage());

        request = new UserLoginRequest(login, password);
        response = authEndpoint.login(request);
        assertNotNull(response.getToken());
        assertTrue(response.isSuccess());

        @NotNull UserLogoutRequest logoutRequest = new UserLogoutRequest(response.getToken());
        @NotNull UserLogoutResponse logoutResponse = authEndpoint.logout(logoutRequest);
        assertTrue(logoutResponse.isSuccess());

        logoutRequest = new UserLogoutRequest(NONE_STR);
        logoutResponse = authEndpoint.logout(logoutRequest);
        assertFalse(logoutResponse.isSuccess());
        assertNotNull(logoutResponse.getMessage());
    }

}
