package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataYamlSaveFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataYamlSaveFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataYamlSaveFasterXMLListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data to YAML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataYamlSaveFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataYamlSaveFasterXMLRequest request = new DataYamlSaveFasterXMLRequest(getToken());
        @NotNull final DataYamlSaveFasterXMLResponse response = domainEndpoint.saveDataYAMLFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
