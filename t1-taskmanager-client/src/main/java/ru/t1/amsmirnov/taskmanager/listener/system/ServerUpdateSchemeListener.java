package ru.t1.amsmirnov.taskmanager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerUpdateSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerUpdateSchemeResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class ServerUpdateSchemeListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "drop-scheme";

    @NotNull
    public static final String DESCRIPTION = "Update server database.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@serverUpdateSchemeListener .isSystemCommand(#consoleEvent.name)")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[UPDATE SERVER DATABASE]");
        @NotNull final ServerUpdateSchemeRequest request = new ServerUpdateSchemeRequest(getToken());
        @NotNull final ServerUpdateSchemeResponse response = serviceLocator.getSystemEndpoint().updateScheme(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
