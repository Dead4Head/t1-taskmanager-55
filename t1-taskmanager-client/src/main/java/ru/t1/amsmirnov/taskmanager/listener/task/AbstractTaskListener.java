package ru.t1.amsmirnov.taskmanager.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ITaskEndpoint;
import ru.t1.amsmirnov.taskmanager.listener.AbstractListener;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;

import java.util.List;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @SneakyThrows
    protected void renderTasks(@NotNull final List<TaskDTO> taskList) {
        int index = 1;
        for (@Nullable final TaskDTO task : taskList) {
            if (task == null) throw new TaskNotFoundException();
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(final TaskDTO task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
