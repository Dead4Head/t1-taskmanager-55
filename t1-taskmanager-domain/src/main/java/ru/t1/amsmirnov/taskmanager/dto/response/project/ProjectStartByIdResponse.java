package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

public final class ProjectStartByIdResponse extends AbstractProjectResponse {

    public ProjectStartByIdResponse() {
    }

    public ProjectStartByIdResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectStartByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
