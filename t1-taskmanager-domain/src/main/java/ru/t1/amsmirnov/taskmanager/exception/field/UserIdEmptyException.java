package ru.t1.amsmirnov.taskmanager.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! UserId is empty...");
    }

}
