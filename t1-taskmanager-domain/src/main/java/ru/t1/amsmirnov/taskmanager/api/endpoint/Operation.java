package ru.t1.amsmirnov.taskmanager.api.endpoint;

import ru.t1.amsmirnov.taskmanager.dto.request.AbstractRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
