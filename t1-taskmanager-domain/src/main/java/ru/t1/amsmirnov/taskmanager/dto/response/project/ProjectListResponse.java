package ru.t1.amsmirnov.taskmanager.dto.response.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

import java.util.List;

public final class ProjectListResponse extends AbstractResultResponse {

    public ProjectListResponse() {
    }

    @Nullable
    private List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

    public ProjectListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public List<ProjectDTO> getProjects() {
        return projects;
    }

    public void setProjects(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

}