package ru.t1.amsmirnov.taskmanager.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends Exception {

    public AbstractException() {

    }

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(
            @NotNull final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
