package ru.t1.amsmirnov.taskmanager.api.service.dto;

import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

public interface ISessionDtoService extends IAbstractUserOwnedDtoService<SessionDTO> {

}
