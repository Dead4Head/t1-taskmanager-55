package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.model.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository() {
        super();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        final String jql = "SELECT t FROM Task t ORDER BY t.created";
        return entityManager.createQuery(jql, Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT t FROM Task t ORDER BY :sort";
        return entityManager.createQuery(jql, Task.class)
                .setParameter("sort", sort)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        final String jql = "SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t.created";
        return entityManager.createQuery(jql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSorted(
            @NotNull final String userId,
            @Nullable final String sort
    ) {
        if (sort == null || sort.isEmpty()) return findAll(userId);
        final String jql = "SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sort";
        return entityManager.createQuery(jql, Task.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String projectId) {
        final String jql = "SELECT t FROM Task t WHERE t.project.id = :projectId ORDER BY t.created";
        return entityManager.createQuery(jql, Task.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        final String jql = "SELECT t FROM Task t WHERE t.user_id = :userId AND t.project.id = :projectId ORDER BY t.created";
        return entityManager.createQuery(jql, Task.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        final String jql = "SELECT t FROM Task t WHERE t.id = :id";
        return entityManager.createQuery(jql, Task.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Nullable
    @Override
    public Task findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jql = "SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id";
        return entityManager.createQuery(jql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Override
    public void removeAll() {
        final String jql = "DELETE FROM Task";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull String userId) {
        final String jql = "DELETE FROM Task t WHERE t.user.id = :userId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jql = "DELETE FROM Task t WHERE t.user_id = :userId AND t.project_id = :projectId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
