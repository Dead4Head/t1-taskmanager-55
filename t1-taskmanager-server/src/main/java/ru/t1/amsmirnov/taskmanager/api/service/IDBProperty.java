package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDBProperty {

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseFormatSql();

    @NotNull
    String getDatabaseCommentsSql();

    @NotNull
    String getDataBaseSecondLvlCash();

    @NotNull
    String getDataBaseFactoryClass();

    @NotNull
    String getDataBaseUseQueryCash();

    @NotNull
    String getDataBaseUseMinPuts();

    @NotNull
    String getDataBaseRegionPrefix();

    @NotNull
    String getDataBaseHazelConfig();


}
