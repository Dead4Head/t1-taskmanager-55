package ru.t1.amsmirnov.taskmanager.api.repository.dto;

import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IAbstractUserOwnedDtoRepository<SessionDTO> {
}
