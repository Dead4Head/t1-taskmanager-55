package ru.t1.amsmirnov.taskmanager.api.repository.dto;

import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IAbstractUserOwnedDtoRepository<ProjectDTO> {
}
