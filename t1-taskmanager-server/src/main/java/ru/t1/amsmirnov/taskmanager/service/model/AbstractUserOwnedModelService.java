package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.model.IUserOwnedService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;
import ru.t1.amsmirnov.taskmanager.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractUserOwnedModelService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>>
        extends AbstractModelService<M, R>
        implements IUserOwnedService<M> {


    public AbstractUserOwnedModelService() {
        super();
    }

    @NotNull
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final User user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        model.setUser(user);
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllSorted(userId, getComparator(comparator));
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            final M model = repository.findOneById(userId, id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(model);
            transaction.commit();
            return model;
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            final M model = repository.findOneById(id);
            if (model == null) throw new ModelNotFoundException();
            repository.remove(model);
            transaction.commit();
            return model;
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.removeAll(userId);
            transaction.commit();
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            repository.findOneById(userId, id);
            return true;
        } catch (final Exception e) {
            return false;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IAbstractUserOwnedRepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId).size();
        } finally {
            entityManager.close();
        }
    }

}
