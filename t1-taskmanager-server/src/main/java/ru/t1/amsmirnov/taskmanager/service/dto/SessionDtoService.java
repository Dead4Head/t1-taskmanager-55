package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ISessionDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.dto.ISessionDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

@Service
public final class SessionDtoService
        extends AbstractUserOwnedModelDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    public SessionDtoService() {
        super();
    }

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository() {
        return context.getBean(ISessionDtoRepository.class);
    }

}
