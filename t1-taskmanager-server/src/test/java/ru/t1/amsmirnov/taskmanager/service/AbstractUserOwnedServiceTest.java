//package ru.t1.amsmirnov.taskmanager.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.junit.AfterClass;
//import org.junit.BeforeClass;
//import ru.t1.amsmirnov.taskmanager.api.service.dto.IUserDtoService;
//import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
//import ru.t1.amsmirnov.taskmanager.enumerated.Role;
//import ru.t1.amsmirnov.taskmanager.service.dto.UserDtoService;
//
//import java.util.UUID;
//
//public abstract class AbstractUserOwnedServiceTest extends AbstractServiceTest {
//
//    @NotNull
//    protected static final String USER_ALFA_ID = UUID.randomUUID().toString();
//
//    @NotNull
//    static final UserDTO USER_ALFA = new UserDTO();
//
//    @NotNull
//    protected static final String USER_BETA_ID = UUID.randomUUID().toString();
//
//    @NotNull
//    static final UserDTO USER_BETA = new UserDTO();
//
//    protected static IUserDtoService USER_SERVICE = new UserDtoService(CONNECTION_SERVICE, PROPERTY_SERVICE);
//
//    @BeforeClass
//    public static void initUsers() throws Exception {
//        USER_ALFA.setId(USER_ALFA_ID);
//        USER_ALFA.setFirstName("First Name");
//        USER_ALFA.setPasswordHash("HASH!!!!");
//        USER_ALFA.setLastName("Last Name ");
//        USER_ALFA.setMiddleName("Middle Name ");
//        USER_ALFA.setEmail("useralfa@tm.ru");
//        USER_ALFA.setLogin("alfa");
//        USER_ALFA.setRole(Role.USUAL);
//
//        USER_BETA.setId(USER_BETA_ID);
//        USER_BETA.setFirstName("First Name");
//        USER_BETA.setPasswordHash("HASH!!!!");
//        USER_BETA.setLastName("Last Name ");
//        USER_BETA.setMiddleName("Middle Name ");
//        USER_BETA.setEmail("userbeta@tm.ru");
//        USER_BETA.setLogin("beta");
//        USER_BETA.setRole(Role.USUAL);
//
//        USER_SERVICE.add(USER_ALFA);
//        USER_SERVICE.add(USER_BETA);
//    }
//
//    @AfterClass
//    public static void clearUsers() {
//        USER_SERVICE.removeAll();
//    }
//
//}
